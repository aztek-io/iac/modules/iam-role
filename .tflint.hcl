# null path will default to "/" (Optional)
#    https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role#path
rule "aws_iam_role_invalid_path" {
  enabled = false
}
