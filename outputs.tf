output "arn" {
  value = aws_iam_role.this.arn
}

output "name" {
  value = local.name
}
