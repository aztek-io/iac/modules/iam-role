resource "random_string" "name" {
  length  = 8
  special = false
  upper   = false
  number  = false
}
